﻿import random
from itertools import combinations
import math
import copy
import gmpy
import sys
import os
import pickle
sys.setrecursionlimit(10000)

def coPrime(l):
    """Returneză ‘True’ dacă valorile în lista l
    sunt toate reciproc prime, altfel, returnează ‘False’ """
    for i, j in combinations(l, 2):
        if gmpy.gcd(i, j) != 1:
            return False
    return True

def modInv(a, m):
    """Returnează inversul multiplicativ a lui a mod m,
    ca o valoare pozitivă între 0 și m-1. """
    # notăm că a și m trebuie să fie reciproc prime.
    if coPrime([a, m]):
        return gmpy.divm(1,a,m)
    else:
        return 0

def genPrime(lenbit):
    """Funcție pentru generarea numerelor prime cu lungimea dată în biți.
    Se utilizează gmpy.is_prime deoarece micșorează colosal
    timpul de procesare. Metodă alternativă este generarea
    numerelor aleatoare și utilizarea testelor la primaritate,
    de exemplu testului Miller-Rabin."""
    isPrime = gmpy.is_prime
    res = 666
    while (not isPrime(res)):
        res = random.getrandbits(lenbit)
    return res

def newKey(size = 1024):
    """Funcție pentru crearea cheilor cu lungimea dată în biți.
    Implicit lungimea este 1024."""
    p = genPrime(size)
    q = genPrime(size)
    n = p * q
    print 'p=',p
    print 'q=',q

    m = (p - 1) * (q - 1)
    while True:
        e = random.randint(1, m)
        if coPrime([e, m]):
            break
    d = modInv(e, m)
    return (n, e, d)


def string2numList(strn):
    """Funcție pentru convertirea unui string în listă de int-uri
    bazat pe valori ASCII"""
    # Notăm că raza de caractere afișabile ASCII este 0x20 - 0x7E
    return [ord(chars) for chars in strn]


def numList2string(l):
    """Convertirea listei de int-uri într-un string bazat pe valorile ASCII."""
    # Notăm că raza de caractere afișabile ASCII este 0x20 - 0x7E
    return ''.join(map(chr, l))


def numList2blocks(l, n):
    """Se ia o listă de numere întregi (fiecare între 0 și 127)
    și se combină în blocuri de lungimea n, utilizînd baza 256.
    Daca len(l) % n != 0, se alege niște valori aleatoare pentru
    a face blocul."""
    # Notăm că raza de caractere afișabile ASCII este 0x20 - 0x7E
    returnList = []
    toProcess = copy.copy(l)
    if len(toProcess) % n != 0:
        for i in range(0, n - len(toProcess) % n):
            toProcess.append(random.randint(32, 126))
    for i in range(0, len(toProcess), n):
        block = 0
        for j in range(0, n):
            block += toProcess[i + j] << (8 * (n - j - 1))
        returnList.append(block)
    return returnList


def blocks2numList(blocks, n):
    """Funcția inversă a funcției numList2blocks."""
    toProcess = copy.copy(blocks)
    returnList = []
    for numBlock in toProcess:
        inner = []
        for i in range(0, n):
            inner.append(numBlock % 256)
            numBlock >>= 8
        inner.reverse()
        returnList.extend(inner)
    return returnList


def encrypt(message, modN, e, blockSize):
    """Se dă un string(mesaj), cheia publică și mărimea dorită
    a blocurilor. Mesajul se encriptează utilizînd algoritmul RSA."""
    numList = string2numList(message)
    numBlocks = numList2blocks(numList, blockSize)
    return [pow(blocks, e, modN) for blocks in numBlocks]


def decrypt(secret, modN, d, blockSize):
    """Funcția inversă a a funcției encrypt.
    Decriptarea conform algoritmului RSA"""
    numBlocks = [pow(blocks, d, modN) for blocks in secret]
    numList = blocks2numList(numBlocks, blockSize)
    return numList2string(numList)

if __name__ == '__main__':
    print'Cheile generate se salveaza in fisierele \
n_key.txt e_key.txt d_key.txt\n\
Pentru a encripta mesajul, introduceti-l in fisierul message.txt\n\
Mesajul criptat se salveaza in fisierul cipher.txt\n\
Pentru a decripta mesajul criptat, salvati-l in fisierul cipher.txt\n\
Mesajul decriptat se va salva in fisierul deciphered.txt'
    x=''
    while x != "x":
        print "\n \n"
        print '========================================'
        print "Generarea cheilor -> g"
        print "Criptarea mesajului -> c"
        print "Decriptarea mesajului -> d"
        print "Instructiuni de utilizare -> h"
        print "Iesire -> x"
        print '========================================'
        x=raw_input("Alegeti optiunea -> ")
        for i in range(80):
            print "\n"
        if x == "g":
            (n, e, d) = newKey()
            if not os.path.exists('d_key.txt'):
                f = open('d_key.txt', 'w+')
                f.write(str(d))
                f.close()
            else:
                print 'Fisierul d_key.txt deja exista! De rescris? y/n'
                k=raw_input()
                if k == "y":
                    f = open('d_key.txt', 'w+')
                    f.write(str(d))
                    f.close()
                if k == "n":
                    break
            f = open('n_key.txt', 'w+')
            f.write(str(n))
            f.close()
            f = open('e_key.txt', 'w+')
            f.write(str(e))
            f.close()
        if x == "c":
            if not os.path.exists('n_key.txt'):
                print 'Lipseste fisierul n_key.txt'
                raw_input('Apasati Enter pentru iesire.')
                break
            f = open("n_key.txt", "r")
            n = int(f.read())
            f.close()
            if not os.path.exists('e_key.txt'):
                print 'Lipseste fisierul e_key.txt'
                raw_input('Apasati Enter pentru iesire.')
                break
            f = open("e_key.txt", "r")
            e = int(f.read())
            f.close()
            if not os.path.exists('message.txt'):
                print 'Lipseste fisierul message.txt'
                raw_input('Apasati Enter pentru iesire.')
                break
            f = open("message.txt", "r")
            message = f.read()
            f.close()

            cipher = encrypt(message, n, e, 15)
            f = open('cipher.txt','w+')
            pickle.dump(cipher, f)
            f.close()
        if x == "d":
            if not os.path.exists('n_key.txt'):
                print 'Lipseste fisierul n_key.txt'
                raw_input('Apasati Enter pentru iesire.')
                break
            f = open("n_key.txt", "r")
            n = int(f.read())
            f.close()
            if not os.path.exists('d_key.txt'):
                print 'Lipseste fisierul d_key.txt'
                raw_input('Apasati Enter pentru iesire.')
                break
            f = open("d_key.txt", "r")
            d = int(f.read())
            f.close()
            if not os.path.exists('cipher.txt'):
                print 'Lipseste fisierul ciphered.txt'
                raw_input('Apasati Enter pentru iesire.')
                break
            f = open("cipher.txt", "r")
            cipher = pickle.load(f)
            f.close()
            deciphered = decrypt(cipher, n, d, 15)
            f = open('deciphered.txt', 'w+')
            f.write(deciphered)
            f.close()
        if x == "h":
            print'Cheile generate se salveaza in fisierele \
n_key.txt e_key.txt d_key.txt\n\
Pentru a encripta mesajul, introduceti-l in fisierul message.txt\n\
Mesajul criptat se salveaza in fisierul cipher.txt\n\
Pentru a decripta mesajul criptat, salvati-l in fisierul cipher.txt\n\
Mesajul decriptat se va salva in fisierul deciphered.txt'
        if x == "x":
            print "Iesire.."







