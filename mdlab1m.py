graph = { 1 : [1,3,4],
          2 : [1, 3],
          3 : [3, 4, 5],
          4 : [5,6],
          5 : [2, 6],
          6 : [],
          7 : [],
          8 : [1,2,3,4,5,6,8]
        }

def input_g():
    graph.clear()
    print "Introduceti nodurile"
    print "Pentru finisare tastati 'q'"
    while 1:
        x=raw_input()
        if x=='q':
            break
        graph[int(x)]=[]
    #print graph
    
    for i in graph:
        print "Introduceti conexiunile pentru nodul "+str(i)+": "
        print "Pentru finisare tastati 'q'"
        while 1:
            p=raw_input()
            if p=='q':
                break
            if int(p) in graph.keys():
                if int(p) in graph[i]:
                    print "Asa vecin a fost deja introdus!"
                else:
                    graph[i].append(int(p))
            else:
                print "Nu exista asa nod!"
                print "Introduceti un nod existent!"
    return graph

def read_g(graph):
    graph.clear()
    f=open('graph.txt','r')
    graph=eval(f.read())
    f.close()
    return graph

def write_g(graph):
    f=open('graph.txt','w+')
    f.write(str(graph))
    f.flush()
    f.close()   


def mat_ad(graph):
    matrix_ad = [[0 for a in range(len(graph))]for b in range(len(graph))]
    for node in graph:
        #print graph[node]
        for i in graph[node]:
            matrix_ad[node-1][i-1] = 1
    return matrix_ad

def afisare(graph):
    for node in graph:
        print node,"->",graph[node]
        
def afisare_mat(graph):
    for node in graph:
        print node

def generate_edges(graph):
    edges = []
    for node in graph:
        for neighbour in graph[node]:
            edges.append((node, neighbour))
    return edges

def mat_in(graph):
    x=generate_edges(graph)
    matrix_in = [[0 for a in range(len(graph))]for b in range(len(x))]
    k=0
    for i in x:
        #print list(i)
        if i[0]==i[1]:
            matrix_in[k][i[1]-1]=2
            k+=1
        else:
            matrix_in[k][i[1]-1]=1
            matrix_in[k][i[0]-1]=-1
            k+=1
            
                
    return matrix_in
            


x='z'
while x != "x":
    print "\n \n"
    print '========================================'
    print "Afisare graph -> p"
    print "Introducere graph nou -> i"
    print "Salvarea graph-ului in fisier -> w"  
    print "Citirea graph-ului din fisier -> r"
    print "Afisarea matricei de adiacenta -> d"    
    print "Afisarea matricei de incidenta -> n"    
    print "Iesire -> x"
    print '========================================'
    x=raw_input("Alegeti optiunea -> ")
    for i in range(80):
        print "\n"    
    if x == "p":
        afisare(graph)
    if x == "i":
        input_g()
    if x == "w":
        write_g(graph)
    if x == "r":
        graph=read_g(graph)
    if x == "d":
        afisare_mat(mat_ad(graph))
    if x == "n":
        afisare_mat(mat_in(graph))
    if x == "x":
        print "Iesire.."
