﻿import pdb
graph = {
        's': {'2': 9, '6': 14, '7': 15},
        '2': {'3': 24},
        '3': {'t': 19, '5': 2},
        '4': {'3': 6, 't': 6},
        '5': {'4': 11, 't': 16},
        '6': {'7': 5, '5': 30, '3': 18},
        '7': {'5': 20, 't': 44},
        't': {}
        }
# Pasul 1: Pentru fiecare nod pregatim destinatia si predecesorul
def initialize(graph, source):
    d = {} # Destinatie
    p = {} # Predecesor
    for node in graph:
        d[node] = float('Inf') # Incepem cu admiterea ca restul nodurilor sunt foarte departe
        p[node] = None
    d[source] = 0 # Pentru sursa, care stim cum sa ajungem la ea
    return d, p

def relax(node, neighbour, graph, d, p):
    #Daca distanta dintre nod si vecin este mai mica decit ceea care o avem acum
    if d[neighbour] > d[node] + graph[node][neighbour]:
        # Inregistram aceasta distanta
        d[neighbour]  = d[node] + graph[node][neighbour]
        p[neighbour] = node

def bellman_ford(graph, source):
    d, p = initialize(graph, source)
    for i in range(len(graph)-1):
        for u in graph:
            for v in graph[u]: #Pentru fiecare vecin al lui u
                relax(u, v, graph, d, p) #Efectuam functia relax

    # Pasul 3: Controlam ciclurile cu cost negativ
    for u in graph:
        for v in graph[u]:
            assert d[v] <= d[u] + graph[u][v]

    return d, p

if __name__ == '__main__':
    print bellman_ford(graph, source = 's')