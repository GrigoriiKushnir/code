import imaplib
import email
import re
import base64
import time
import dateparser

USERNAME = # username
PASSWORD = # password
IMAP = 'imap.gmail.com'


list_headers = list()

mail = imaplib.IMAP4_SSL(IMAP)
mail.login(USERNAME, PASSWORD)
print(mail.list())
for i in mail.list()[1]:
    l = i.decode().split(' "/" ')
    print(l[0] + " = " + l[1].replace('"', ''))

mail.select("&BBoEPgRABDcEOAQ9BDA-")
result, data = mail.search(None, "ALL")

ids = data[0]
id_list = ids.split()

for email_id in reversed(id_list):
    if int(email_id.decode()) == int(id_list[-1].decode()) - 3:
        break
    prefix = '=?UTF-8?B?'
    suffix = '?='
    header = mail.fetch(email_id, '(BODY[HEADER.FIELDS (FROM SUBJECT DATE)])')
    data = header[1][0][1].decode()
    from_data = re.search('From.*', data)
    subject_data = re.search('Subject.*', data)
    date = re.search('Date.*', data).group(0)
    date = date[6:-1]
    print(date)
    c = 0
    for i in date:
        if i == '(':
            date = date[:c - 1]
        c += 1
    print(date)
    if '-0000' in date:
        date = date[:-5]
    date_datetime = dateparser.parse(date)
    date = date_datetime.strftime('%H:%M - %d %b %Y')
    from_data = from_data.group(0)
    subject_data = subject_data.group(0)
    if prefix in from_data:
        email = ""
        if '<' and '>' in from_data:
            email = re.search('<.*?>', from_data)
            email = email.group(0)
        from_data = re.search('=\?UTF-8.*?=', from_data).group(0)[len(prefix):len(from_data) - len(suffix)]
        from_data = base64.b64decode(from_data).decode()
        if email:
            from_data = from_data + ' ' + email
    if prefix in subject_data:
        subject_data = re.search('=\?UTF-8.*?=', subject_data).group(0)[len(prefix):len(subject_data)]
        try:
            subject_data = base64.b64decode(subject_data).decode()
        except Exception as exc:
            print(exc)
    if '\r' in subject_data:
        subject_data = subject_data.replace('\r', '')
    if '\r' in from_data:
        from_data = from_data.replace('\r', '')
    from_data = from_data.replace('From: ', '')
    if '<' and '>' in from_data:
        start = from_data.index('<') + len('>')
        end = from_data.index('>', start)
        from_data = from_data[start:end]
    list_headers.append([from_data, subject_data[:30].replace('Subject: ', ''), date])
print(list_headers)
