from smtplib import SMTP_SSL as SMTP
from PyQt5 import QtCore, QtGui, QtWidgets
from secondWindow import Ui_SecondWindow
import base64
import ssl
from socket import *


class Ui_MainWindow(object):

    def login_check(self):
        SMTPserver = 'smtp.gmail.com'
        USERNAME = self.lineEdit.text()
        PASSWORD = self.lineEdit_2.text()
        with open("cred.txt", 'wb') as file:
            file.write(base64.b64encode(USERNAME.encode()) + "\n".encode() + base64.b64encode(PASSWORD.encode()))

        cc = socket(AF_INET, SOCK_STREAM)
        cc.connect(("smtp.gmail.com", 465))

        scc = ssl.wrap_socket(cc, ssl_version=ssl.PROTOCOL_TLS)
        scc.send('helo gris.kushnir\r\n'.encode())
        response = scc.recv(1000).decode("utf-8")

        scc.send('auth login\r\n'.encode())
        response = scc.recv(1000).decode("utf-8")
        response = scc.recv(1000).decode("utf-8")

        scc.send((base64.b64encode(USERNAME.encode()) + '\r\n'.encode()))
        response = scc.recv(1000).decode("utf-8")
        scc.send(base64.b64encode(PASSWORD.encode()) + '\r\n'.encode())
        response = scc.recv(1000).decode("utf-8")
        print(response)

        if "235" in response:
            self.secondWindow = QtWidgets.QMainWindow()
            self.ui = Ui_SecondWindow()
            self.ui.setupUi(self.secondWindow)
            self.secondWindow.show()
            MainWindow.hide()
        elif response == "":
            self.label_4.setText("No internet!")
        else:
            self.label_4.setText("Wrong credentials!")

        # try:
        #     conn = SMTP(SMTPserver)
        #     conn.set_debuglevel(False)
        #     try:
        #         conn.login(USERNAME, PASSWORD)
        #         result = 1
        #     finally:
        #         conn.quit()
        # except Exception as exc:
        #     if str(exc).find('Authentication failed'):
        #         self.label_4.setText("Wrong credentials!")
        #     else:
        #         self.label_4.setText(exc)
        # if result:
        #     self.secondWindow = QtWidgets.QMainWindow()
        #     self.ui = Ui_SecondWindow()
        #     self.ui.setupUi(self.secondWindow)
        #     self.secondWindow.show()
        #     MainWindow.hide()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(500, 300)
        MainWindow.setMinimumSize(QtCore.QSize(500, 300))
        MainWindow.setMaximumSize(QtCore.QSize(500, 300))

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setMinimumSize(QtCore.QSize(500, 300))
        self.centralwidget.setObjectName("centralwidget")

        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(170, 30, 191, 41))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(36)
        self.label.setFont(font)
        self.label.setTextFormat(QtCore.Qt.PlainText)
        self.label.setObjectName("label")

        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(120, 100, 71, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")

        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(110, 140, 81, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")

        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(200, 165, 150, 21))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_4.setFont(font)
        self.label_4.setStyleSheet('color: red')
        self.label_4.setObjectName("label_4")

        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(190, 190, 151, 41))
        self.pushButton.setObjectName("pushButton")

        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(210, 100, 141, 21))
        self.lineEdit.setText("")
        self.lineEdit.setObjectName("lineEdit")

        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_2.setGeometry(QtCore.QRect(210, 140, 141, 21))
        self.lineEdit_2.setText("")
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.lineEdit_2.setEchoMode(QtWidgets.QLineEdit.Password)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 500, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.pushButton.clicked.connect(self.login_check)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "FreeMail"))
        self.label.setText(_translate("MainWindow", "FreeMail"))
        self.label_2.setText(_translate("MainWindow", "Account"))
        self.label_3.setText(_translate("MainWindow", "Password"))
        self.label_4.setText(_translate("MainWindow", ""))
        self.pushButton.setText(_translate("MainWindow", "Enter"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

