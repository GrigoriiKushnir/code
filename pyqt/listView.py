from PyQt5 import QtCore, QtGui, QtWidgets
import imaplib
import re
import base64
import time

class Ui_listView(object):
    def setupUi(self, listView):
        listView.setObjectName("listView")
        listView.resize(737, 490)
        self.listWidget = QtWidgets.QListWidget(listView)
        self.listWidget.setGeometry(QtCore.QRect(20, 40, 691, 441))
        self.listWidget.setObjectName("listWidget")
        self.label = QtWidgets.QLabel(listView)
        self.label.setGeometry(QtCore.QRect(330, 0, 91, 31))
        font = QtGui.QFont()
        font.setPointSize(22)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(listView)
        self.pushButton.setGeometry(QtCore.QRect(20, 10, 75, 23))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(listView)
        self.pushButton_2.setGeometry(QtCore.QRect(110, 10, 75, 23))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(listView)
        self.pushButton_3.setGeometry(QtCore.QRect(630, 10, 81, 23))
        self.pushButton_3.setObjectName("pushButton_3")

        self.retranslateUi(listView)
        QtCore.QMetaObject.connectSlotsByName(listView)
        listView.setMinimumHeight(30)

    def retranslateUi(self, listView):
        _translate = QtCore.QCoreApplication.translate
        listView.setWindowTitle(_translate("listView", "Form"))
        self.label.setText(_translate("listView", "Inbox"))
        self.pushButton.setText(_translate("listView", "Refresh"))
        self.pushButton_2.setText(_translate("listView", "View"))
        self.pushButton_3.setText(_translate("listView", "Delete"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    listView = QtWidgets.QWidget()
    ui = Ui_listView()
    ui.setupUi(listView)
    listView.show()
    sys.exit(app.exec_())

