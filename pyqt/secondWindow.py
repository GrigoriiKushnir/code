import sys
from smtplib import SMTP_SSL as SMTP
import imaplib
from email.mime.text import MIMEText
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QThread
import ssl
from socket import *
import re
import base64
import time
from warning import Ui_Dialog
from viewMessage import Ui_Form


class AThread(QThread):

    warning = QtCore.pyqtSignal(object)

    def __init__(self, destination, subject, content):
        super().__init__()
        self.destination = destination
        self.subject = subject
        self.content = content

    def run(self):
        print("enter")
        SMTPserver = 'smtp.gmail.com'
        with open("cred.txt") as f:
            lines = f.readlines()
        # username = base64.b64decode(lines[0]).decode()
        # password = base64.b64decode(lines[1]).decode()
        # sender = username
        # text_subtype = 'plain'
        # if not self.destination:
        #     self.warning.emit(("Warning",))
        # else:
        #     try:
        #         msg = MIMEText(self.content, text_subtype)
        #         msg['Subject'] = self.subject
        #         msg['From'] = sender  # some SMTP servers will do this automatically, not all
        #
        #         conn = SMTP(SMTPserver)
        #         conn.set_debuglevel(False)
        #         conn.login(username, password)
        #         try:
        #             conn.sendmail(sender, self.destination, msg.as_string())
        #             self.warning.emit(("Success",))
        #         except Exception as ex:
        #             self.warning.emit(("Error1", ex))
        #             print(ex)
        #             conn.quit()
        #     except Exception as exc:
        #         self.warning.emit(("Error2",exc))
        #         print(exc)
        #         pass

        username = lines[0][:-1].encode()
        decoded_user = base64.b64decode(lines[0]).decode()
        password = lines[1].encode()
        crlf = '\r\n'.encode()
        # Im sorry for this
        MAIL_FROM = 'MAIL FROM:'.encode() + '<'.encode() + base64.b64decode(lines[0]) + '>'.encode() + crlf
        RCPT_TO = 'RCPT TO:'.encode() + '<'.encode() + self.destination.encode() + '>'.encode() + crlf
        DATA = 'From: '.encode() + decoded_user.encode() + crlf + 'Subject: '.encode() + self.subject.encode() + crlf + \
            self.content.encode() + crlf

        if not self.destination:
            self.warning.emit(("Warning",))
        else:
            cc = socket(AF_INET, SOCK_STREAM)
            cc.connect(("smtp.gmail.com", 465))

            scc = ssl.wrap_socket(cc, ssl_version=ssl.PROTOCOL_TLS)
            scc.send('helo '.encode() + decoded_user.split('@')[0].encode() + crlf)
            response = scc.recv(1000).decode("utf-8")
            print(response)

            scc.send('auth login\r\n'.encode())
            response = scc.recv(1000).decode("utf-8")
            print(response)
            response = scc.recv(1000).decode("utf-8")
            print(response)

            scc.send(username + '\r\n'.encode())
            response = scc.recv(1000).decode("utf-8")
            print(response)
            scc.send(password + '\r\n'.encode())
            response = scc.recv(1000).decode("utf-8")
            print(response)
            scc.send(MAIL_FROM)
            response = scc.recv(1000).decode("utf-8")
            print(response)
            scc.send(RCPT_TO)
            response = scc.recv(1000).decode("utf-8")
            if "is not a valid" in response:
                self.warning.emit(("Error1", "Not a valid recipient address!"))
            print(response)
            scc.send('DATA\r\n'.encode())
            scc.send(DATA)
            scc.send('.\r\n'.encode())
            response = scc.recv(1000).decode("utf-8")
            response = scc.recv(1000).decode("utf-8")
            print(response)
            if "OK" in response:
                self.warning.emit(("Success",))


class listThread(QThread):

    update_signal = QtCore.pyqtSignal(object)
    button_signal = QtCore.pyqtSignal(object)

    def __init__(self):
        super().__init__()

    def run(self):
        with open("cred.txt") as f:
            lines = f.readlines()
        username = base64.b64decode(lines[0]).decode()
        password = base64.b64decode(lines[1]).decode()

        mail = imaplib.IMAP4_SSL('imap.gmail.com')
        mail.login(username, password)
        mail.list()

        mail.select("inbox")

        result, data = mail.search(None, "ALL")

        ids = data[0]

        id_list = ids.split()
        for email_id in reversed(id_list):
            prefix = '=?UTF-8?B?'
            suffix = '?='
            # result, data = mail.fetch(email_id, "(RFC822)")
            header = mail.fetch(email_id, '(BODY[HEADER.FIELDS (FROM SUBJECT DATE)])')
            data = header[1][0][1].decode()
            # print(data)
            from_data = re.search('From.*', data)
            subject_data = re.search('Subject.*', data)
            date = re.search('Date.*', data).group(0)
            date = date[6:-16]
            try:
                conv = time.strptime(date, "%a, %d %b %Y")
                date = time.strftime("%d/%m/%Y", conv)
            except Exception:
                conv = time.strptime(date, "%a, %d %b %Y %H:%M")
                date = time.strftime("%d/%m/%Y", conv)

            from_data = from_data.group(0)
            subject_data = subject_data.group(0)
            if prefix in from_data:
                email = ""
                if '<' and '>' in from_data:
                    email = re.search('<.*?>', from_data)
                    email = email.group(0)
                from_data = re.search('=\?UTF-8.*?=', from_data).group(0)[len(prefix):len(from_data) - len(suffix)]
                from_data = base64.b64decode(from_data).decode()
                if email:
                    from_data = from_data + ' ' + email
            if prefix in subject_data:
                subject_data = re.search('=\?UTF-8.*?=', subject_data).group(0)[len(prefix):len(subject_data)]
                try:
                    subject_data = base64.b64decode(subject_data).decode()
                except Exception as exc:
                    pass
            # Bydlocode incoming!!!
            if "From: " in from_data:
                from_data = from_data.replace("From: ", "")
            if "Subject: " in subject_data:
                subject_data = subject_data.replace("Subject: ", "")
            self.update_signal.emit("          ".join((from_data, subject_data, date)))
        self.button_signal.emit("Emitted")
        mail.close()


class viewThread(QThread):

    view_signal = QtCore.pyqtSignal(object)

    def __init__(self, email_id):
        super().__init__()
        self.email_id = email_id

    def run(self):
        with open("cred.txt") as f:
            lines = f.readlines()
        username = base64.b64decode(lines[0]).decode()
        password = base64.b64decode(lines[1]).decode()

        mail = imaplib.IMAP4_SSL('imap.gmail.com')
        mail.login(username, password)
        mail.list()

        mail.select("inbox")
        result, data = mail.search(None, "ALL")
        ids = data[0]
        id_list = ids.split()
        email_id = id_list[-self.email_id-1]

        result, data = mail.fetch(email_id, "(RFC822)")
        raw_email = data[0][1].decode("utf-8")
        self.view_signal.emit(raw_email)
        mail.close()

class deleteThread(QThread):

    delete_signal = QtCore.pyqtSignal(object)

    def __init__(self, email_id):
        super().__init__()
        self.email_id = email_id

    def run(self):
        with open("cred.txt") as f:
            lines = f.readlines()
        username = base64.b64decode(lines[0]).decode()
        password = base64.b64decode(lines[1]).decode()

        mail = imaplib.IMAP4_SSL('imap.gmail.com')
        mail.login(username, password)
        mail.list()

        mail.select("inbox")
        result, data = mail.search(None, "ALL")
        ids = data[0]
        id_list = ids.split()
        email_id = id_list[-self.email_id-1]
        try:
            mail.store(email_id, '+FLAGS', '\\Deleted')
            mail.expunge()
            self.delete_signal.emit("Deleted")
            mail.close()
        except Exception as exc:
            print(exc)


class Ui_SecondWindow(object):

    def send_message(self):
        destination = self.lineTo.text()
        content = self.textEdit.toPlainText()
        self.textEdit.clear()
        subject = self.lineSubject.text()
        try:
            self.t1 = AThread(destination, subject, content)
            self.t1.warning.connect(self.handle_signal)
            self.t1.start()
        except Exception as exc:
            print(exc)

    def view_message(self):
        item = self.listWidget.selectedIndexes()
        if item:
            try:
                for it in item:
                    row = it.row()
                    self.t1 = viewThread(row)
                    self.t1.view_signal.connect(self.handle_view)
                    self.t1.start()
            except Exception as exc:
                print(exc)

    def delete_message(self):
        item = self.listWidget.selectedIndexes()
        if item:
            for it in item:
                row = it.row()
            try:
                self.t1 = deleteThread(row)
                self.t1.delete_signal.connect(self.handle_delete)
                self.t1.start()
            except Exception as exc:
                print(exc)

    def handle_view(self, passed):
        self.viewMessage = QtWidgets.QTextBrowser()
        self.viewMessageUi = Ui_Form()
        self.viewMessageUi.setupUi(self.viewMessage)
        self.viewMessageUi.textBrowser.setText(passed)
        self.viewMessage.show()

    def handle_delete(self, passed):
        self.get_inbox()

    def handle_signal(self, passed):
        if passed[0] == "Warning":
            self.warning = QtWidgets.QDialog()
            self.ui = Ui_Dialog()
            self.ui.setupUi(self.warning)
            self.warning.show()
        if passed[0] == "Success":
            self.labelStatus.setStyleSheet('color: green')
            self.labelStatus.setText("Message sent successfully!")
        if passed[0] == "Error1":
            self.labelStatus.setStyleSheet('color: red')
            self.labelStatus.setText(str(passed[1]))
        if passed[0] == "Error2":
            self.labelStatus.setText(passed[1])

    def get_inbox(self):
        self.listWidget.clear()
        self.refreshButton.setDisabled(1)
        self.deleteButton.setDisabled(1)
        try:
            self.t1 = listThread()
            self.t1.update_signal.connect(self.handle_update)
            self.t1.button_signal.connect(self.handle_refresh)
            self.t1.start()
        except Exception as exc:
            print(exc)

    def handle_update(self, passed):
        self.listWidget.addItem(passed)

    def handle_refresh(self, passed):
        self.refreshButton.setDisabled(0)
        self.deleteButton.setDisabled(0)


    def setupUi(self, SecondWindow):
        SecondWindow.setObjectName("SecondWindow")
        SecondWindow.resize(811, 522)
        SecondWindow.setMaximumWidth(811)
        SecondWindow.setMaximumHeight(522)

        self.centralwidget = QtWidgets.QWidget(SecondWindow)
        self.centralwidget.setObjectName("centralwidget")


        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(-4, -1, 821, 511))
        self.tabWidget.setObjectName("tabWidget")

        """INBOX"""
        self.tabInbox = QtWidgets.QWidget()
        self.tabInbox.setObjectName("tabInbox")
        self.tabWidget.addTab(self.tabInbox, "")

        self.listWidget = QtWidgets.QListWidget(self.tabInbox)
        self.listWidget.setGeometry(QtCore.QRect(20, 40, 780, 440))
        self.listWidget.setObjectName("listWidget")


        self.labelInbox = QtWidgets.QLabel(self.tabInbox)
        self.labelInbox.setGeometry(QtCore.QRect(375, 0, 91, 31))
        font = QtGui.QFont()
        font.setPointSize(22)
        self.labelInbox.setFont(font)
        self.labelInbox.setObjectName("label")

        self.refreshButton = QtWidgets.QPushButton(self.tabInbox)
        self.refreshButton.setGeometry(QtCore.QRect(20, 10, 75, 23))
        self.refreshButton.setObjectName("Refresh")

        self.viewButton = QtWidgets.QPushButton(self.tabInbox)
        self.viewButton.setGeometry(QtCore.QRect(110, 10, 75, 23))
        self.viewButton.setObjectName("View")

        self.deleteButton = QtWidgets.QPushButton(self.tabInbox)
        self.deleteButton.setGeometry(QtCore.QRect(720, 10, 81, 23))
        self.deleteButton.setObjectName("Delete")

        """SEND"""
        self.tabSend = QtWidgets.QWidget()
        self.tabSend.setObjectName("tabSend")
        self.tabWidget.addTab(self.tabSend, "")

        self.labelTo = QtWidgets.QLabel(self.tabSend)
        self.labelTo.setGeometry(QtCore.QRect(90, 80, 31, 16))
        font = QtGui.QFont()
        font.setPointSize(14)

        self.labelTo.setFont(font)
        self.labelTo.setObjectName("labelTo")

        self.pushButton = QtWidgets.QPushButton(self.tabSend)
        self.pushButton.setGeometry(QtCore.QRect(340, 420, 151, 41))
        self.pushButton.setObjectName("sendButton")

        self.lineSubject = QtWidgets.QLineEdit(self.tabSend)
        self.lineSubject.setGeometry(QtCore.QRect(140, 120, 571, 20))
        self.lineSubject.setObjectName("lineSubject")

        self.textEdit = QtWidgets.QTextEdit(self.tabSend)
        self.textEdit.setGeometry(QtCore.QRect(140, 150, 571, 181))
        self.textEdit.setObjectName("textEdit")

        self.labelWrite = QtWidgets.QLabel(self.tabSend)
        self.labelWrite.setGeometry(QtCore.QRect(320, 0, 191, 61))
        font = QtGui.QFont()
        font.setPointSize(22)
        self.labelWrite.setFont(font)
        self.labelWrite.setObjectName("labelWrite")

        self.lineTo = QtWidgets.QLineEdit(self.tabSend)
        self.lineTo.setGeometry(QtCore.QRect(140, 80, 571, 20))
        self.lineTo.setObjectName("lineTo")

        self.labelSubject = QtWidgets.QLabel(self.tabSend)
        self.labelSubject.setGeometry(QtCore.QRect(50, 110, 71, 31))
        font = QtGui.QFont()
        font.setPointSize(14)

        self.labelSubject.setFont(font)
        self.labelSubject.setObjectName("labelSubject")

        self.labelStatus = QtWidgets.QLabel(self.tabSend)
        self.labelStatus.setGeometry(QtCore.QRect(146, 350, 561, 20))
        self.labelStatus.setText("")
        self.labelStatus.setObjectName("labelStatus")
        self.labelStatus.setStyleSheet('color: green')

        SecondWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(SecondWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 811, 21))
        self.menubar.setObjectName("menubar")
        SecondWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(SecondWindow)
        self.statusbar.setObjectName("statusbar")
        SecondWindow.setStatusBar(self.statusbar)

        self.retranslateUi(SecondWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(SecondWindow)

        self.pushButton.clicked.connect(self.send_message)
        self.refreshButton.clicked.connect(self.get_inbox)
        self.viewButton.clicked.connect(self.view_message)
        self.deleteButton.clicked.connect(self.delete_message)
        self.get_inbox()


    def retranslateUi(self, SecondWindow):
        _translate = QtCore.QCoreApplication.translate
        SecondWindow.setWindowTitle(_translate("SecondWindow", "FreeMail"))
        self.labelTo.setText(_translate("SecondWindow", "To:"))
        self.labelWrite.setText(_translate("SecondWindow", "Write Message"))
        self.labelSubject.setText(_translate("SecondWindow", "Subject:"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabSend), _translate("SecondWindow", "Send"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabInbox), _translate("SecondWindow", "Inbox"))
        self.pushButton.setText("Send")
        self.labelInbox.setText(_translate("SecondWindow", "Inbox"))
        self.refreshButton.setText(_translate("SecondWindow", "Refresh"))
        self.viewButton.setText(_translate("SecondWindow", "View"))
        self.deleteButton.setText(_translate("SecondWindow", "Delete"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    SecondWindow = QtWidgets.QMainWindow()
    ui = Ui_SecondWindow()
    ui.setupUi(SecondWindow)
    SecondWindow.show()

    sys.exit(app.exec_())

