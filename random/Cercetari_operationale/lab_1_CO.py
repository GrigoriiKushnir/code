﻿from sympy import symbols, diff
from sympy.solvers import solve
from numpy import matrix

a = 2
b = 5

def gradient(a, b):

    x, y, k = symbols('x y k', real=True)
    initial = [1,1]
    epsilon = 10**-5

    f = a*x**2 + 2*x*y + b*y**2 - 2*x - 3*y

    dx = f.diff(x).subs({x:1, y:1})
    dy = f.diff(y).subs({x:1, y:1})

    #print dx
    #print dy


    x1 = initial[0]-dx*k
    x2 = initial[1]-dy*k
    z = x1
    w = x2

    fi = diff(a*z**2 + 2*z*w + b*w**2 - 2*z - 3*w, k)
    print 'fi = ', fi

    c = solve(fi, k)
    coeff = c[0]
    #coeff = 0.5
    #print 'coeff = ', coeff

    x0 = initial[0]
    y0 = initial[1]

    cont = 0

    while True:

        m0 = matrix((x0,y0))

        dx = f.diff(x).subs({x:x0, y:y0})
        dy = f.diff(y).subs({x:x0, y:y0})
##
##        m1 = coeff * matrix((dx,dy))
##        m = m0 - m1
##
##        #if (a*float(m.item(0))**2 + 2*float(m.item(0))*float(m.item(1)) + b*float(m.item(1))**2 - 2*float(m.item(0)) - 3*float(m.item(1)) > a*x0**2 + 2*x0*y0 + b*y0**2 - 2*x0 - 3*y0 ):
##            #coeff = coeff / 2.
##
##        if (x0-float(m.item(0)) < epsilon and y0 - float(m.item(1)) < epsilon):
##            break
##
##        #if (cont > 10):
##            #break
##
##        x0 = float(m.item(0))
##        y0 = float(m.item(1))
##        cont += 1
##        print m
##    print 'cont = ', cont
    return 0


print gradient(a,b)
