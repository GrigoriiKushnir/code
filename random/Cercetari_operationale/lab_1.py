﻿from sympy import symbols, diff
from sympy.solvers import solve
from numpy import matrix

a = 2
b = 5

def gradient(a, b):

    x, y, k = symbols('x y k', real=True)
    initial = [1,1]
    epsilon = 10**-5

    f = a*x**2 + 2*x*y + b*y**2 - 2*x - 3*y

    dx = f.diff(x).subs({x:1, y:1})
    dy = f.diff(y).subs({x:1, y:1})

    #print dx
    #print dy


    x1 = initial[0]-dx*k
    x2 = initial[1]-dy*k
    z = x1
    w = x2

    fi = diff(a*z**2 + 2*z*w + b*w**2 - 2*z - 3*w, k)
    #print fi

    c = solve(fi, k)
    coeff = c[0]
    #print coeff


    x0 = initial[0]
    y0 = initial[1]

    while True:

        m0 = matrix((x0,y0))

        dx = f.diff(x).subs({x:x0, y:y0})
        dy = f.diff(y).subs({x:x0, y:y0})

        m1 = coeff * matrix((dx,dy))
        m = m0 - m1
        if (x0-float(m.item(0)) < epsilon and y0 - float(m.item(1)) < epsilon):
            break
        x0 = float(m.item(0))
        y0 = float(m.item(1))
        print m
    return 0

print gradient(a,b)
