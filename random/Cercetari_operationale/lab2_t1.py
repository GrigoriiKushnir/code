﻿from sympy import symbols, diff
from sympy.solvers import solve
from numpy import matrix, int32
from math import exp
from decimal import Decimal

a = 2
b = 7
e = 2.71828
# f = (exp(x**2+y**2))*(a*x**2+b*y**2)

def gradient(a, b):

    x, y, k = symbols('x y k', real=True)

    initial = [1,1]
    epsilon = 10**-5
    e = 2.71828

    f = (e**(x**2+y**2))*(a*x**2+b*y**2)

    dx = f.diff(x).subs({x:1, y:1})
    dy = f.diff(y).subs({x:1, y:1})

    print dx
    print dy


    x1 = initial[0]-dx*k
    x2 = initial[1]-dy*k
    z = x1
    w = x2

    fi = diff((e**(z**2+w**2))*(a*z**2+b*w**2), k)
    print 'fi = ', fi

##    c = solve(fi, k)
##    coeff = c[0]
    coeff = 0.5
    print 'coeff = ', coeff


    x0 = initial[0]
    y0 = initial[1]

    cont = 0

    while True:

        m0 = matrix((x0,y0))

        dx = f.diff(x).subs({x:x0, y:y0})
        dy = f.diff(y).subs({x:x0, y:y0})

        m1 = coeff * matrix((dx,dy))
        m = m0 - m1

##        if (e**(Decimal(m.item(0))**2 + Decimal(m.item(0)))*(a*(Decimal(m.item(0)))**2 + b*(Decimal(m.item(1))))**2 > e**(Decimal*x0)**2+Decimal(y0)**2)*(a*Decimal(x0)**2+b*Decimal(y0)**2):
        coeff = coeff / 2.

##        if (x0-float(m.item(0)) < epsilon and y0 - float(m.item(1)) < epsilon):
##            break

        if (cont > 10):
            break

        x0 = int(m.item(0))
        y0 = int(m.item(1))
        cont += 1
        print m
    print 'cont = ', cont
    return 0


print gradient(a,b)
