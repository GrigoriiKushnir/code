﻿from scipy import optimize
import numpy as np

args = (2,7)

e = 2.71828

def f(x, *args):
    u, v = x
    a, b = args
    return (e**(u**2+v**2))*(a*u**2+b*v**2)

def gradf(x, *args):
    u, v = x
    a, b = args
    gu = (2*u*e**(u**2+v**2))*(a*u**2+a+b*v**2)
    gv = (2*v*e**(u**2+v**2))*(a*u**2+b*v**2+b)
    return np.asarray((gu, gv))


x0 = np.asarray((1, 1))
opts = {'maxiter' : None,    # default value.
         'disp' : True,    # non-default value.
         'gtol' : 1e-5,    # default value.
         'norm' : np.inf,  # default value.
         'eps' : 1.4901161193847656e-08}  # default value.

res2 = optimize.minimize(f, x0, jac=gradf, args=args,method='SLSQP', options=opts)

