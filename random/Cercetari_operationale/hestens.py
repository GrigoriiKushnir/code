﻿import numpy as np
import itertools

A = np.matrix([[4.0, 2.0], [2.0, 10.0]])
b = np.matrix([[2.0], [3.0]])

x = np.matrix([[1.0],[1.0]])

i = 0
imax = 2

g = b - A * x
g1 = g
d = g

while i < imax:
    alpha = float(g.T * d / float(d.T * (A * d)))
    x = x + alpha * d
    g = b - A * x

    beta = -float((g.T * A * d) / float(d.T * A * d))
    #beta = (g.T * g) / (g1 * g1.T) (nu lucreaza in 2 iteratii)

    d = g + beta * d
    g1 = g
    i += 1
    print 'i = ',i
    print 'x = ',x