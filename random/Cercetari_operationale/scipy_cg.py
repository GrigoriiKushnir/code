﻿from scipy import optimize
import numpy as np

args = (2,7)

e = 2.71828

def f(x, *args):
    u, v = x
    a, b = args
    return (e**(u**2+v**2))*(a*u**2+b*v**2)

def gradf(x, *args):
    u, v = x
    a, b = args
    gu = (2*u*e**(u**2+v**2))*(a*u**2+a+b*v**2)
    gv = (2*v*e**(u**2+v**2))*(a*u**2+b*v**2+b)
    return np.asarray((gu, gv))

x0 = np.asarray((2, 2))

res1 = optimize.fmin_cg(f, x0, fprime=gradf, args=args)
print res1
