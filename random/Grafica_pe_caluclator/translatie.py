﻿import pygame, sys
from pygame.locals import *
import math

def main():
    pygame.init()

    DISPLAY=pygame.display.set_mode((600,600),0,32)

    WHITE=(255,255,255)
    blue=(0,0,255)

    DISPLAY.fill(WHITE)

    pygame.draw.polygon(DISPLAY, (0,0,255),((50,250),(100,250),(100,350)),2)
    pygame.draw.rect (DISPLAY, (0,0,255), Rect((300,270), (50,100)),2)

    tr_x = 150 # la 200 se lipeste
    tr_y = 20

    pygame.draw.polygon(DISPLAY, (0,0,255),((50+tr_x,250 +tr_y),(100+tr_x,250 +tr_y),(100+tr_x,350 +tr_y)),2)
    pygame.draw.rect (DISPLAY, (0,0,255), Rect((300+tr_x,270 +tr_y), (50,100)),2)
    pygame.display.flip()


    while True:
        for event in pygame.event.get():
            if event.type==QUIT:
                pygame.quit()
                sys.exit()
        pygame.display.update()

main()