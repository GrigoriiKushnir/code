﻿import pygame, sys
from pygame.locals import *
import math

def main():
    pygame.init()

    SCALARE_P=pygame.display.set_mode((600,600),0,32)

    WHITE=(255,255,255)
    blue=(0,0,255)

    SCALARE_P.fill(WHITE)

    pygame.draw.polygon(SCALARE_P, (0,0,255),((50,250),(100,250),(100,350)),2)
    pygame.draw.rect (SCALARE_P, (0,0,255), Rect((300,270), (50,50)),2)

    scale = 1.3

    pygame.draw.polygon(SCALARE_P, (0,0,255),((50*scale+50-50*scale,250*scale+250-250*scale),(100*scale+50-50*scale,250*scale+250-250*scale),(100*scale+50-50*scale,350*scale+250-250*scale)),2)
    pygame.draw.rect (SCALARE_P, (0,0,255), Rect((300,270), (50*scale,50*scale)),2)
    pygame.display.flip()


    while True:
        for event in pygame.event.get():
            if event.type==QUIT:
                pygame.quit()
                sys.exit()
        pygame.display.update()

main()