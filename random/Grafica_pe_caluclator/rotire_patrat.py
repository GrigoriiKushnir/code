﻿import pygame, sys
from pygame.locals import *
import math

def rotatePoint(centerPoint,point,angle):
    x = point[0]
    y = point[1]
    xf = centerPoint[0]
    yf = centerPoint[1]
    a = math.radians(angle)
    rotated_point = (x*math.cos(a)-y*math.sin(a)+xf-xf*math.cos(a)+yf*math.sin(a),x*math.sin(a)+y*math.cos(a)+yf-xf*math.sin(a)-yf*math.cos(a))
    return rotated_point

w_w = 500
w_h = 500
centerPoint = (w_w/2,w_h/2)
rect_wh = 100
d = 135

p1 = (w_w/2-rect_wh/2, w_h/2-d-rect_wh/2)
p2 = (w_w/2-rect_wh/2, w_h/2-d+rect_wh/2)

p3 = (w_w/2+rect_wh/2, w_h/2-d+rect_wh/2)
p4 = (w_w/2+rect_wh/2, w_h/2-d-rect_wh/2)
my_polygon = [p1,p2,p3,p4]


pygame.init()

DISPLAY=pygame.display.set_mode((w_w,w_h),0,32)
WHITE=(255,255,255)
blue=(0,0,255)
DISPLAY.fill(WHITE)
i = 0
for angle in range(0,360,30):
    i+=10
    pygame.draw.polygon(DISPLAY,(100+i,0,255-i),(rotatePoint(centerPoint,my_polygon[0],angle),rotatePoint(centerPoint,my_polygon[1],angle),rotatePoint(centerPoint,my_polygon[2],angle),rotatePoint(centerPoint,my_polygon[3],angle)),3)
pygame.display.flip()

while True:
    for event in pygame.event.get():
        if event.type==QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()

