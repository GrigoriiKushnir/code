﻿import pygame, sys
from pygame.locals import *
import math

##def rotatePolygon(polygon,theta):
##    """Rotates the given polygon which consists of corners represented as (x,y),
##    around the ORIGIN, clock-wise, theta degrees"""
##    theta = math.radians(theta)
##    rotatedPolygon = []
##    for corner in polygon :
##        rotatedPolygon.append(( corner[0]*math.cos(theta)-corner[1]*math.sin(theta) , corner[0]*math.sin(theta)+corner[1]*math.cos(theta)) )
##    return rotatedPolygon

def rotatePoint(centerPoint,point,angle):
    angle = math.radians(angle)
    temp_point = point[0]-centerPoint[0] , point[1]-centerPoint[1]
    temp_point = ( temp_point[0]*math.cos(angle)-temp_point[1]*math.sin(angle) , temp_point[0]*math.sin(angle)+temp_point[1]*math.cos(angle))
    temp_point = temp_point[0]+centerPoint[0] , temp_point[1]+centerPoint[1]
    return temp_point

w_w = 800
w_h = 800
centerPoint = (w_w/2,w_h/2)
rect_wh = 100
d = 135

p1 = (w_w/2-rect_wh/2, w_h/2-d-rect_wh/2)
p2 = (w_w/2-rect_wh/2, w_h/2-d+rect_wh/2)

p3 = (w_w/2+rect_wh/2, w_h/2-d+rect_wh/2)
p4 = (w_w/2+rect_wh/2, w_h/2-d-rect_wh/2)
my_polygon = [p1,p2,p3,p4]


pygame.init()

DISPLAY=pygame.display.set_mode((w_w,w_h),0,32)
WHITE=(255,255,255)
blue=(0,0,255)
DISPLAY.fill(WHITE)

for angle in range(0,360,30):
    pygame.draw.polygon(DISPLAY,blue,(rotatePoint(centerPoint,my_polygon[0],angle),rotatePoint(centerPoint,my_polygon[1],angle),rotatePoint(centerPoint,my_polygon[2],angle),rotatePoint(centerPoint,my_polygon[3],angle)),2)
##for angle in range(0,360,30):
##    pygame.draw.polygon(DISPLAY,blue,(rotatePolygon(my_polygon,-angle)[0],rotatePolygon(my_polygon,-angle)[1],rotatePolygon(my_polygon,-angle)[2],rotatePolygon(my_polygon,-angle)[3]),1)
##for angle in range(0,360,30):
##    pygame.draw.polygon(DISPLAY,blue,(rotatePolygon(my_polygon,-angle)[0],rotatePolygon(my_polygon,-angle)[1],rotatePolygon(my_polygon,-angle)[2],rotatePolygon(my_polygon,-angle)[3]),1)
pygame.display.flip()
while True:
    for event in pygame.event.get():
        if event.type==QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()

