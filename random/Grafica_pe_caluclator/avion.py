﻿import pygame, sys
from pygame.locals import *
import math
import time

def rotate(centerPoint, point, angle):
    x = point[0]
    y = point[1]
    xf = centerPoint[0]
    yf = centerPoint[1]
    a = math.radians(angle)
    rotated_point = (x*math.cos(a)-y*math.sin(a)+xf-xf*math.cos(a)+yf*math.sin(a),x*math.sin(a)+y*math.cos(a)+yf-xf*math.sin(a)-yf*math.cos(a))
    return rotated_point

pygame.init()

w_w = 600
w_h = 600
centerPoint = (w_w/2,w_h/2)

DISPLAY=pygame.display.set_mode((w_w,w_h),0,32)

WHITE=(255,255,255)
blue=(0,0,255)

DISPLAY.fill(WHITE)

origine = (0,0)
avion1 = []
avion =[(-30,0),(-5,30),(-5,40),(0,45),(5,40),(5,30),(30,0),(5,20),(5,-30),(15,-40),(0,-35),(-15,-40),(-5,-30),(-5,20)] #14 puncte

for p in avion:
    avion1.append(rotate(origine,p,-90))

avion2 = []
for p in avion1:
    avion2.append((p[0]+centerPoint[0],p[1]+centerPoint[1]+150))

avion_a = avion2
for i in range(180):
    pygame.draw.polygon(DISPLAY, (255,255,255),((avion_a)),2)
    avion_a = []
    for p in avion2:
        avion_a.append(rotate(centerPoint,p,i*-2))
    pygame.draw.polygon(DISPLAY, (0,0,255),((avion_a)),2)
    pygame.display.flip()
    time.sleep(0.05)
    pygame.display.flip()

while True:
    for event in pygame.event.get():
        if event.type==QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()
