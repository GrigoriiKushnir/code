﻿import pygame, sys
from pygame.locals import *
import math

def rotate(centerPoint, point, angle):
    x = point[0]
    y = point[1]
    xf = centerPoint[0]
    yf = centerPoint[1]
    a = math.radians(angle)
    rotated_point = (x*math.cos(a)-y*math.sin(a)+xf-xf*math.cos(a)+yf*math.sin(a),x*math.sin(a)+y*math.cos(a)+yf-xf*math.sin(a)-yf*math.cos(a))
    return rotated_point

pygame.init()

w_w = 800
w_h = 800

DISPLAY=pygame.display.set_mode((w_w,w_h),0,32)

WHITE=(255,255,255)
blue=(0,0,255)

p1 = [(50,250),(50,300),(100,300),(100,250)]
#p1 = [(160,250),(110,270),(130,340),(150,350)]


p2 = []
p3 = []
for p in p1:
    p2.append((p[1],p[0]))

for p in p1:
    p3.append((w_w-p[1],w_h-p[0]))

print p2

DISPLAY.fill(WHITE)

pygame.draw.line(DISPLAY, (0, 0, 0), (w_w/2, 0), (w_w/2, w_w),3) # axa Y
pygame.draw.line(DISPLAY, (0, 0, 0), (0, w_h/2), (w_h, w_h/2),3) # axa X

pygame.draw.line(DISPLAY, (0, 0, 255), (0, 0), (w_w, w_h))
pygame.draw.line(DISPLAY, (0, 0, 255), (w_w, 0), (0, w_w))

pygame.draw.polygon(DISPLAY, (0,0,255),(p1),2)
pygame.draw.polygon(DISPLAY, (0,255,0),(p2),2)
pygame.draw.polygon(DISPLAY, (255,0,0),(p3),2)

pygame.display.flip()


while True:
    for event in pygame.event.get():
        if event.type==QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()
