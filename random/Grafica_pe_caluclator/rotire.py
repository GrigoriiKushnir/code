﻿import pygame, sys
from pygame.locals import *
import math

def rotate(centerPoint, point, angle):
    x = point[0]
    y = point[1]
    xf = centerPoint[0]
    yf = centerPoint[1]
    a = math.radians(angle)
    rotated_point = (x*math.cos(a)-y*math.sin(a)+xf-xf*math.cos(a)+yf*math.sin(a),x*math.sin(a)+y*math.cos(a)+yf-xf*math.sin(a)-yf*math.cos(a))
    return rotated_point

pygame.init()

DISPLAY=pygame.display.set_mode((600,600),0,32)

WHITE=(255,255,255)
blue=(0,0,255)

DISPLAY.fill(WHITE)

pygame.draw.polygon(DISPLAY, (0,0,255),((50,250),(100,250),(100,350)),2)


centerPoint = (50,250)
angle = 300
p1 = (50,250)
p2 = (100,250)
p3 = (100,350)
pygame.draw.polygon(DISPLAY, (0,0,255),(rotate(centerPoint, p1, angle),rotate(centerPoint, p2, angle),rotate(centerPoint, p3, angle)),2)

pp1 = (300,270)
pp2 = (300,320)
pp3 = (350,320)
pp4 = (350,270)
pygame.draw.polygon(DISPLAY, (0,0,255),(pp1,pp2,pp3,pp4),2)
centerPoint1 = (300,270)
pygame.draw.polygon(DISPLAY, (0,0,255),(rotate(centerPoint1, pp1, angle),rotate(centerPoint1, pp2, angle),rotate(centerPoint1, pp3, angle),rotate(centerPoint1, pp4, angle)),2)
pygame.display.flip()


while True:
    for event in pygame.event.get():
        if event.type==QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()
