import pygame, sys
from pygame.locals import *
import math

def main():
    pygame.init()

    DISPLAY=pygame.display.set_mode((600,600),0,32)

    WHITE=(255,255,255)
    blue=(0,0,255)

    DISPLAY.fill(WHITE)

    pygame.draw.line(DISPLAY, (0, 0, 255), (0, 200), (600, 200))
    pygame.draw.line(DISPLAY, (0, 0, 255), (0, 400), (600, 400))
    pygame.draw.line(DISPLAY, (0, 0, 255), (200, 0), (200, 600))
    pygame.draw.line(DISPLAY, (0, 0, 255), (400, 0), (400, 600))
    pygame.draw.aaline(DISPLAY, (0, 100, 100), (300, 50), (350, 150))
    pygame.draw.circle(DISPLAY, (0,0,255), (500, 100), 50, 1)
    pygame.draw.polygon(DISPLAY, (0,0,255),((50,250),(100,250),(100,350)))

    pygame.draw.rect (DISPLAY, (140,240,130), Rect((250,270), (50,50)),1)
    pygame.draw.rect (DISPLAY, (140,255,255), Rect((320,310), (50,50)),1)
    pygame.draw.line(DISPLAY, (0, 0, 255), (250, 320), (320, 360))
    pygame.draw.line(DISPLAY, (0, 0, 255), (250, 270), (320, 310))
    pygame.draw.line(DISPLAY, (0, 0, 255), (300, 270), (370, 310))
    pygame.draw.line(DISPLAY, (0, 0, 255), (370, 360), (300, 320))

    pygame.draw.rect (DISPLAY, (140,240,130), Rect((500,270), (50,100)),1)

    pygame.draw.polygon(DISPLAY, (0,0,255),((10,500),(100,480),(100,550),(110,480),(120,490),(130,500),(130,590),(100,600),(70,590),(80,580),(70,570),(50,500),(40,510),(5,500),(3,500)),1)


    #pygame.draw.arc(DISPLAY, (0,150,0),(10,20,150,100), 4, 6)
    #pygame.draw.ellipse(DISPLAY, (0,0,255), [225, 10, 50, 20], 2)
    pygame.draw.circle(DISPLAY, (0,0,255), (100, 100), 50, )
    pygame.draw.line(DISPLAY, (0, 0, 255), (100, 100), (100, 150))
    pygame.draw.line(DISPLAY, (0, 0, 255), (100, 100), (150, 100))
    pygame.draw.rect (DISPLAY, (255,255,255), Rect((101,101), (50,50)),0)

    pygame.draw.ellipse(DISPLAY, (0,255,0), [250, 500, 100, 50])


    pygame.draw.arc(DISPLAY, (0,255,0), [425, 450, 150, 125],  1.59, 3.45, 2)
    pygame.draw.line(DISPLAY, (0, 0, 255), (500, 512.5), (500, 450),2)
    pygame.draw.line(DISPLAY, (0, 0, 255), (500, 512.5), (430, 530),2)
    #pygame.draw.arc(DISPLAY, (255,255,255), [425, 450, 150, 125],  2, 6)

##    pygame.draw.arc(DISPLAY, (0,255,0), [425, 450, 150, 125],  0, math.pi/2, 2)
##    pygame.draw.line(DISPLAY, (0, 0, 255), (500, 512.5), (500, 450),2)
##    pygame.draw.line(DISPLAY, (0, 0, 255), (500, 512.5), (575, 512.5),2)
    pygame.display.flip()

    fname = "imagine.bmp"
    pygame.image.save(DISPLAY, fname)
    print("file {} has been saved".format(fname))


    pygame.display.flip()


    while True:
        for event in pygame.event.get():
            if event.type==QUIT:
                pygame.quit()
                sys.exit()
        pygame.display.update()

main()