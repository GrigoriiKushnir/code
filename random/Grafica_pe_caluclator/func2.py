﻿import pygame, sys
from pygame.locals import *
from pygame import gfxdraw
import math
pygame.init()
screen = pygame.display.set_mode((600,600))
screen.fill((255,255,255))
pygame.display.flip()
black = (0,0,0)

##def bresenham(x1,y1,x2,y2):
##	dx = x2-x1
##	dy = y2-y1
##
##	D = 2*dy - dx
##	gfxdraw.pixel(screen,x1,y1,black)
##	y = round(math.sin(x1))
##
##	for x in range(x1+1,x2+1):
##		if D > 0:
##			y += 1
##			gfxdraw.pixel(screen,x,y,black)
##			D += (2*dy-2*dx)
##		else:
##			gfxdraw.pixel(screen,x,y,black)
##			D += 2*dy
##	pygame.display.flip()
##
##bresenham(10,10,50,50)
getmaxx = 100
getmaxy = 100

a = 2
ymin = -a
ymax = a
xmin = -10
xmax = 1
h = 0.01
cx = (getmaxx-10)/(xmax-xmin)
jmax = getmaxy-5
cy = jmax/(ymax-ymin)
i = int(round(-xmin*cx))
j = jmax-int(round(-ymin*cy))
x = xmin
while (x<=xmax):
    y = a*math.sin(x)
    i = int(round(cx*(x-xmin)))
    j = jmax-int(round(cy*(y-ymin)))
    gfxdraw.pixel(screen,i,j,black)
    x = x + h


getmaxx = 600
getmaxy = 100

a = 10
ymin = -4
ymax = 40
xmin = -10
xmax = 9
h = 0.01
cx = (getmaxx-100)/(xmax-xmin)
jmax = getmaxy-10
cy = jmax/(ymax-ymin)
i = int(round(-xmin*cx))
j = jmax-int(round(-ymin*cy))
x = xmin
while (x<=xmax):
    y = a*math.cos(x)
    i = int(round(cx*(x-xmin)))
    j = jmax-int(round(cy*(y-ymin)))
    gfxdraw.pixel(screen,i,j,black)
    x = x + h

getmaxx = 600
getmaxy = 200

a = 10
ymin = -a
ymax = a
xmin = -50
xmax = 1
h = 0.01
cx = (getmaxx-15)/(xmax-xmin)
jmax = getmaxy-5
cy = jmax/(ymax-ymin)
i = int(round(-xmin*cx))
j = jmax-int(round(-ymin*cy))
x = xmin
while (x<=xmax):
    y = math.cos(x)+pow(math.sin(x),2)
    i = int(round(cx*(x-xmin)))
    j = jmax-int(round(cy*(y-ymin)))
    gfxdraw.pixel(screen,i,j,black)
    x = x + h

while True:
    for event in pygame.event.get():
        if event.type==QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()