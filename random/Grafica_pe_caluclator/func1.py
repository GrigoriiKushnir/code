﻿import pygame, sys
from pygame.locals import *
from pygame import gfxdraw
import math

pygame.init()
screen = pygame.display.set_mode((800,600))
screen.fill((255,255,255))
pygame.display.flip()
black = (0,0,0)

def bresenham(x,y,x2,y2):
    steep = 0
    dx = abs(x2 - x)
    if (x2 - x) > 0: sx = 1
    else: sx = -1
    dy = abs(y2 - y)
    if (y2 - y) > 0: sy = 1
    else: sy = -1
    if dy > dx:
        steep = 1
        x,y = y,x
        dx,dy = dy,dx
        sx,sy = sy,sx
    d = (2 * dy) - dx
    for i in range(0,dx):
        if steep: gfxdraw.pixel(screen,y,x,black)
        else: gfxdraw.pixel(screen,x,y,black)
        while d >= 0:
            y = y + sy
            d = d - (2 * dx)
        x = x + sx
        d = d + (2 * dy)
    gfxdraw.pixel(screen,x2,y2,black)
    pygame.display.flip()



x1 = 0
y1 = 0
x = 0
y = 0
for i in range(400):
    x1 = x1 + 0.1
    y1 = int(round(30*(math.cos(x1)+pow(math.sin(x1),2))))
    #print int(x*10),y+300
    bresenham(int(x*10),y+300,int(x1*10),y1+300)
    x = x1
    y = y1

x1 = 0
y1 = 0
x = 0
y = 0
for i in range(400):
    x1 = x1 + 0.1
    y1 = int(round(30*(math.cos(x1))))
    #print int(x*10),y+100
    bresenham(int(x*10),y+400,int(x1*10),y1+400)
    x = x1
    y = y1

x1 = 0
y1 = 0
x = 0
y = 0
for i in range(400):
    x1 = x1 + 0.1
    y1 = int(round(30*(math.sin(x1))))
    #print int(x*10),y+100
    bresenham(int(x*10),y+500,int(x1*10),y1+500)
    x = x1
    y = y1

rect = screen.get_rect()
screen1 = pygame.transform.flip(screen,False, True)
screen.blit(screen1,rect)
pygame.display.flip()

while True:
    for event in pygame.event.get():
        if event.type==QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()