﻿import scipy
import sympy
import numpy as np
import matplotlib.pyplot as plt
import sys

def Lagrange(Lx, Ly):
    X=sympy.symbols('X')
    #X = 2.5
    if  len(Lx)!= len(Ly):
        print "ERROR"
        return 1
    y=0
    for k in range (len(Lx)):
        t=1
        for j in range (len(Lx)):
            if j != k:
                t=t* ((X-Lx[j]) /(Lx[k]-Lx[j]))
        y += t*Ly[k]
    return y

Lx=[1,2,3]
Ly=[1,8,27]
points =[(1,1),(2,8),(3,27)]

def plot(f, points):
	x = range(0, 10)
	y = map(f, x)
	#print y
	plt.plot( x, y, linewidth=2.0)
	x_list = []
	y_list = []
	for x_p, y_p in points:
		x_list.append(x_p)
		y_list.append(y_p)
	#print x_list
	#print y_list
	plt.plot(x_list, y_list,  'ro')

	plt.show()

def lagrange(points):
	def P(x):
		total = 0
		n = len(points)
		for i in xrange(n):
			xi, yi = points[i]

			def g(i, n):
				tot_mul = 1
				for j in xrange(n):
					if i == j:
						continue
					xj, yj = points[j]
					tot_mul *= (x - xj) / float(xi - xj)
				return tot_mul

			total += yi * g(i, n)
		return total
	return P


print Lagrange(Lx,Ly)
print sympy.simplify(Lagrange(Lx,Ly))

P = lagrange(points)
plot(P, points)
