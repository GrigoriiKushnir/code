﻿import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from heapq import heappop, heappush
from itertools import count
from networkx.utils import UnionFind, not_implemented_for
import time
import random
from collections import defaultdict
from heapq import *

NUM_V = 5
#vertices = [x for x in range(NUM_V)]
vertices = list("ABCDE")
edges = []
for i in range(NUM_V):
    for j in range(NUM_V):
        if i != j:
            edges.append((random.randint(10,300), i, j))

graph = {'vertices' : vertices, 'edges' : list(edges)}
#print graph

def prim( nodes, edges ):
    conn = defaultdict( list )
    for n1,n2,c in edges:
        conn[ n1 ].append( (c, n1, n2) )
        conn[ n2 ].append( (c, n2, n1) )

    mst = []
    used = set( nodes[ 0 ] )
    usable_edges = conn[ nodes[0] ][:]
    heapify( usable_edges )

    while usable_edges:
        cost, n1, n2 = heappop( usable_edges )
        if n2 not in used:
            used.add( n2 )
            mst.append( ( n1, n2, cost ) )

            for e in conn[ n2 ]:
                if e[ 2 ] not in used:
                    heappush( usable_edges, e )
    return mst

print vertices
print edges
print "prim:", prim( vertices, edges )
##
###test
##nodes = list("ABCDEFG")
##edges = [ ("A", "B", 7), ("A", "D", 5),
##          ("B", "C", 8), ("B", "D", 9), ("B", "E", 7),
##      ("C", "E", 5),
##      ("D", "E", 15), ("D", "F", 6),
##      ("E", "F", 8), ("E", "G", 9),
##      ("F", "G", 11)]
##
##print "prim:", prim( nodes, edges )