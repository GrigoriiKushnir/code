﻿import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from heapq import heappop, heappush
from itertools import count
from networkx.utils import UnionFind, not_implemented_for
import time

n = 100; m = 4950

# select some edge destinations
L = np.random.choice(xrange(n), 2*m)
# and suppose that each edge has a weight
weights = 0.5 + 5 * np.random.rand(m)

# create a graph object, add n nodes to it, and the edges
G = nx.Graph()
G.add_nodes_from(xrange(n))
for i, (fr, to) in enumerate(zip(L[1::2], L[::2])):
  G.add_edge(fr, to, weight=weights[i])

def kruskal_mst(G, weight='weight'):
    T = nx.Graph(nx.minimum_spanning_edges(G, weight=weight, data=True))
    # Add isolated nodes
    if len(T) != len(G):
        T.add_nodes_from([n for n, d in G.degree().items() if d == 0])
    # Add node and graph attributes as shallow copy
    for n in T:
        T.node[n] = G.node[n].copy()
    T.graph = G.graph.copy()
    return T

start = time.time()
N = kruskal_mst(G)
end = time.time()
print 'krus:',end - start


def prim_mst(G, weight='weight'):
    T = nx.Graph(nx.prim_mst_edges(G, weight=weight, data=True))
    # Add isolated nodes
    if len(T) != len(G):
        T.add_nodes_from([n for n, d in G.degree().items() if d == 0])
    # Add node and graph attributes as shallow copy
    for n in T:
        T.node[n] = G.node[n].copy()
    T.graph = G.graph.copy()
    return T

start = time.time()
V = prim_mst(G)
end = time.time()
print 'prim:',end - start

##N=prim_mst(G)
##
##layout = nx.circular_layout(N)  # or whatever layout you wish
##nx.draw_circular(N)   # draw the graph, must be consistent with layout above
##edgeLabels = {}  # dictionary of node tuples to edge labels: {(nodeX, nodeY): aString}
##for a, b in N.edges():     # loop over all the edges
##    edgeLabels[(a, b)] = str(N.get_edge_data(a, b, {"weight":0})["weight"])   # retrieve the edge data dictionary
##
##nx.draw_networkx_edge_labels(N,pos=layout, edge_labels=edgeLabels) # draw the edge labels
##plt.show()   # show the plotting window