﻿import time
import random
import sys
from heapq import merge
sys.setrecursionlimit(100000)

def quickSort(a):
    if len(a) <= 1:
        return a
    else:
        less = []
        more = []
        pivot = a[len(a)/2]
        for i in a:
            if i < pivot:
                less.append(i)
            if i > pivot:
                more.append(i)
        less = quickSort(less)
        more = quickSort(more)
        return less + [pivot] * a.count(pivot) + more

def merge_sort(m):
    if len(m) <= 1:
        return m

    middle = len(m) // 2
    left = m[:middle]
    right = m[middle:]

    left = merge_sort(left)
    right = merge_sort(right)
    return list(merge(left, right))

def bubble_sort(sep):
    seq = list(sep)
    changed = True
    while changed:
        changed = False
        for i in xrange(len(seq) - 1):
            if seq[i] > seq[i+1]:
                seq[i], seq[i+1] = seq[i+1], seq[i]
                changed = True
    return seq


print "==========Caz mediu=============="
array = [random.randint(1,10000) for _ in range(1000000)]
start = time.time()
med = bubble_sort(array)
end = time.time()
print end-start
#print med

##print "==========Caz favorabil============"
##start = time.time()
##merge_sort(med)
##end = time.time()
##print end-start
##
##print "==========Caz defavorabil==========="
##med.reverse()
##start = time.time()
##merge_sort(med)
##end = time.time()
##print end-start