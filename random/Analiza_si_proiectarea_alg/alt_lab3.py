﻿import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from random import randint
import time

N = 100
G=nx.Graph()

for i in range(N):
    for j in range(N-1):
        G.add_edge(i,j, weight=randint(1,20))


def kruskal_mst(G, weight='weight'):
    T = nx.Graph(nx.minimum_spanning_edges(G, weight=weight, data=True))
    # Add isolated nodes
    if len(T) != len(G):
        T.add_nodes_from([n for n, d in G.degree().items() if d == 0])
    # Add node and graph attributes as shallow copy
    for n in T:
        T.node[n] = G.node[n].copy()
    T.graph = G.graph.copy()
    return T

start = time.time()
N = kruskal_mst(G)
end = time.time()
print 'krus:',end - start

def prim_mst(G, weight='weight'):
    T = nx.Graph(nx.prim_mst_edges(G, weight=weight, data=True))
    # Add isolated nodes
    if len(T) != len(G):
        T.add_nodes_from([n for n, d in G.degree().items() if d == 0])
    # Add node and graph attributes as shallow copy
    for n in T:
        T.node[n] = G.node[n].copy()
    T.graph = G.graph.copy()
    return T

start = time.time()
V = prim_mst(G)
end = time.time()
print 'prim:',end - start

##layout = nx.circular_layout(G)  # or whatever layout you wish
##nx.draw_circular(G)   # draw the graph, must be consistent with layout above
##edgeLabels = {}  # dictionary of node tuples to edge labels: {(nodeX, nodeY): aString}
##for a, b in G.edges():     # loop over all the edges
##    edgeLabels[(a, b)] = str(G.get_edge_data(a, b, {"weight":0})["weight"])   # retrieve the edge data dictionary
##
##nx.draw_networkx_edge_labels(G,pos=layout, edge_labels=edgeLabels) # draw the edge labels
##plt.show()   # show the plotting window