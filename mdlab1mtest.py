graph = { 1 : [1,3,4],
          2 : [1, 3],
          3 : [3, 4, 5],
          4 : [4,5,8],
          5 : [3,4],
          6 : [1,2,8],
          8 : [1,2,3,4,5,8]
        }

def input_g():
    graph.clear()
    print "Introduceti numarul de noduri: ",
    x=int(raw_input())
    for i in range(1,x+1):
        graph[int(i)]=[]
    print graph

    for i in graph:
        print "\nIntroduceti conexiunile pentru nodul "+str(i)+": "
        print "Pentru finisare tastati 'q'"
        while 1:
            p=raw_input()
            if p=='q':
                break
            if int(p) in graph.keys():
                if int(p) in graph[i]:
                    print "Asa vecin a fost deja introdus!"
                else:
                    graph[i].append(int(p))
            else:
                print "Nu exista asa nod!"
                print "Introduceti un nod existent!"

def normalize():
    last_k=graph.keys()[-1]
    for i in range(1,last_k):
        if i not in graph.keys():
            graph[i]=graph[last_k]
            for node in graph:
                if last_k in graph[node]:
                    graph[node].remove(last_k)
                    graph[node].append(i)
            delete(last_k)

def read_g():
    graph.clear()
    f=open('graph.txt','r')
    graph=eval(f.read())
    f.close()

def write_g():
    f=open('graph.txt','w+')
    f.write(str(graph))
    f.flush()
    f.close()

def edit():
    print "Introduceti nodul care doriti sa-l editati: ",
    i=int(raw_input())
    graph[i]=[]
    print "\nIntroduceti conexiunile pentru nodul "+str(i)+": "
    print "Pentru finisare tastati 'q'"
    while 1:
        p=raw_input()
        if p=='q':
            break
        if int(p) in graph.keys():
            if int(p) in graph[i]:
                print "Asa vecin a fost deja introdus!"
            else:
                graph[i].append(int(p))
        else:
            print "Nu exista asa nod!"
            print "Introduceti un nod existent!"

def delete(i):
    del graph[i]
    for node in graph:
        if i in graph[node]:
            graph[node].remove(i)

def add_node():
    i=len(graph)+1
    graph[int(i)]=[]
    print "\nIntroduceti conexiunile pentru nodul "+str(i)+": "
    print "Pentru finisare tastati 'q'"
    while 1:
        p=raw_input()
        if p=='q':
            break
        if int(p) in graph.keys():
            if int(p) in graph[i]:
                print "Asa vecin a fost deja introdus!"
            else:
                graph[i].append(int(p))
        else:
            print "Nu exista asa nod!"
            print "Introduceti un nod existent!"

def mat_ad():
    matrix_ad = [[0 for a in range(len(graph))]for b in range(len(graph))]
    for node in graph:
        #print graph[node]
        for i in graph[node]:
            matrix_ad[node-1][i-1] = 1
    return matrix_ad

def afisare():
    for node in graph:
        print node,"->",graph[node]


def afisare_ad(m_ad):
    print '    ',
    for i in range(1,len(m_ad)+1):
        print str(i)+' ',
    print
    y=1
    for node in m_ad:
        print str(y)," ",node
        y+=1

def afisare_in(lst):
    edges=lst[0]
    m_in=lst[1]
    print '         ',
    for i in range(1,len(m_in[0])+1):
        print str(i)+' ',
    print
    for i in range(1,len(m_in)):
        print edges[i],' ',m_in[i]

def generate_edges():
    edges = []
    for node in graph:
        for neighbour in graph[node]:
            edges.append((node, neighbour))
    return edges

def mat_in():
    x=generate_edges()
    matrix_in = [[0 for a in range(len(graph))]for b in range(len(x))]
    k=0
    for i in x:
        #print list(i)
        if i[0]==i[1]:
            matrix_in[k][i[1]-1]=2
            k+=1
        else:
            matrix_in[k][i[1]-1]=1
            matrix_in[k][i[0]-1]=-1
            k+=1
        lst=[x,matrix_in]

    return lst

x=''
while x != "x":
    print "\n \n"
    print '========================================'
    print "Afisare graph -> p"
    print "Introducere graph nou -> i"
    print "Adaugare nod -> a"
    print "Editare nod -> e"
    print "Eliminare nod -> l"
    print "Salvarea graph-ului in fisier -> w"
    print "Citirea graph-ului din fisier -> r"
    print "Afisarea matricei de adiacenta -> d"
    print "Afisarea matricei de incidenta -> n"
    print "Iesire -> x"
    print '========================================'
    x=raw_input("Alegeti optiunea -> ")
    for i in range(80):
        print "\n"
    if x == "p":
        normalize()
        afisare()
    if x == "i":
        input_g()
    if x == "a":
        add_node()
        afisare()
    if x == "e":
        edit()
        afisare()
    if x == "l":
        print "Introduceti nodul care doriti sa-l eliminati: ",
        i=int(raw_input())
        delete(i)
        normalize()
        afisare()
    if x == "w":
        write_g()
    if x == "r":
        graph=read_g()
        normalize()
        afisare()
    if x == "d":
        afisare_ad(mat_ad())
    if x == "n":
        afisare_in(mat_in())
    if x == "x":
        print "Iesire.."
