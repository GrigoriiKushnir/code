﻿import networkx as nx
import matplotlib.pyplot as plt
import pygraphviz as pgv

def main():
    G=nx.Graph()

    G.add_edge('a','b',weight=0.6)
    G.add_edge('a','c',weight=0.2)
    G.add_edge('c','d',weight=0.1)
    G.add_edge('c','e',weight=0.7)
    G.add_edge('c','f',weight=0.9)
    G.add_edge('a','d',weight=0.3)

    heaviest_path = max(path for path in nx.all_simple_paths(G, source, dest),
                    key=lambda path: get_weight(path))

##    graph = { 'A' : ['A', 'C', 'D'],
##              'B' : ['A', 'C'],
##              'C' : ['C', 'D', 'E'],
##              'D' : ['E'],
##              'E' : ['B'],
##              'F' : [],
##              'G' : [],
##              'H' : ['A', 'B', 'C', 'D', 'E', 'F', 'H']
##            }
##
##    G=nx.DiGraph(graph)

if __name__ == '__main__':
    main()