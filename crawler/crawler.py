import requests
from bs4 import BeautifulSoup as BS
from multiprocessing.pool import ThreadPool
import time

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'}


def crawl_this(url):
    links = []
    raw_links = set()
    parts = url.split('//', 1)
    url_domain = ''.join((parts[0], '//', parts[1].split('/', 1)[0]))
    # print(url_domain)
    try:
        get_response = requests.get(url, headers)
    except ConnectionError:
        print("Connection failed!")
    soup = BS(get_response.text, "html.parser")
    found = soup.find_all('a')
    for a in found:
        try:
            if a['href'].startswith('/'):
                raw_links.add(''.join((url_domain, a['href'])))
            else:
                raw_links.add(a['href'])
        except KeyError:
            pass
    for link in raw_links:
        if link.startswith('http'):
            links.append(link)
    return get_response.text, links


def crawl(url, max_depth, word):
    url = url
    word = word
    max_depth = max_depth
    word_found = False
    all_links = [url]
    visited = []
    depth = 0
    while depth < max_depth and all_links != [] and not word_found:
        depth += 1
        url = all_links[0]
        print(depth)
        print(url)
        all_links = all_links[1:]
        if url not in visited:
            data, links = crawl_this(url)
            if data.find(word) > -1:
                word_found = True
                print("Success!")
                break
            all_links += links
            visited.append(url)
    if word_found:
        print("The word ", word, "found at ", url)
    else:
        print("Word not found!")


def get_code(link):
    return requests.get(url=link, headers=headers).status_code


def main():
    url = 'http://utm.md'
    word = 'go'
    max_depth = 100
    crawl(url, max_depth, word)

if __name__ == "__main__":
    main()
