from bs4 import BeautifulSoup as BS
from threading import Thread, Lock
import urllib.request
import queue
import requests
import time

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'}
q = queue.Queue()
qf = queue.Queue()
tq = queue.Queue()
visited = []
found_urls = set()
count = 0
lock = Lock()


def put_urls(url):
    raw_links = set()
    parts = url.split('//', 1)
    url_domain = ''.join((parts[0], '//', parts[1].split('/', 1)[0]))
    try:
        get_response = requests.get(url, headers).text
    except ConnectionError:
        pass
    soup = BS(get_response, "html.parser")
    found = soup.find_all('a')
    for a in found:
        try:
            if a['href'].startswith('//'):
                raw_links.add(a['href'])
            elif a['href'].startswith('/'):
                raw_links.add(''.join((url_domain, a['href'])))
            elif a['href'].endswith('.pdf') or a['href'].endswith('.docx'):
                pass
            else:
                raw_links.add(a['href'])
        except KeyError:
            pass
    for link in raw_links:
        if link.startswith('http'):
            q.put(link)
    return get_response


def crawl(word):
    global count
    while count < 1000:  # check that the queue isn't empty
        with lock:
            count += 1
            url = q.get()  # get the item from the queue
        if url not in visited:
            text = put_urls(url)
            visited.append(url)
        if text.find(word) > -1:
            qf.put(url)
        print(count, url, text.find(word))
        q.task_done()  # specify that you are done with the item


def main():
    start = time.time()
    url = 'http://goodmedia.md/index.php?module=servicii&menu=2'
    word = "grisha"
    visited.append(url)
    put_urls(url)
    for i in range(10):  # aka number of threads
        t = Thread(target=crawl, args=(word,))  # target is the above function
        t.start()  # start the thread
        tq.put(t)
    while not tq.empty():
        tq.get().join()
    end = time.time()
    # while not qf.empty():
    #     print(qf.get())
    print(end-start)

if __name__ == "__main__":
    main()
