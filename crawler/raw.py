import urllib.request
from urllib.parse import urlencode
import urllib
import re
from multiprocessing.pool import ThreadPool
import time

user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'
headers = {'User-Agent': user_agent}


def get_code(link):
    request = urllib.request.Request(link, None, headers)
    return urllib.request.urlopen(request, cafile='kickboxing.crt').getcode()


def get_data(link):
    request = urllib.request.Request(link, None, headers)
    return urllib.request.urlopen(request, cafile='kickboxing.crt').read()


def main():
    the_page = get_data('https://kickboxing.md')

    matches = re.findall(r'<img[^>]*\ssrc="(.*?)"', str(the_page))

    links = []
    for match in matches:
        if match.startswith('/'):
            links.append(''.join(('https://kickboxing.md', match)))
        else:
            links.append(match)

    # checking response using multithreading
    # start = time.time()
    # how_many = 10
    # pool = ThreadPool(processes=how_many)
    # codes = pool.map(func=get_code, iterable=links)
    # pool.close()
    # pool.join()
    # end = time.time()
    # print(end - start)
    #
    # for code in codes:
    #     if code != 200:
    #         print("Some links may not work!")

    # # downloading image
    # f = open('00000001.jpg', 'wb')
    # f.write(get_data(links[3]))
    # f.close()

    url = 'https://www.kickboxing.md/'
    post_fields = {'searchword': 'cusnir', 'task': 'search',
                   'option': 'com_search', 'Itemid': '73'}
    request = urllib.request.Request(url, urlencode(post_fields).encode(), headers)
    html = urllib.request.urlopen(request, cafile='kickboxing.crt').read().decode()
    print(html)

if __name__ == "__main__":
    main()
