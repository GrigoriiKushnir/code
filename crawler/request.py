import requests
from bs4 import BeautifulSoup as BS
import shutil

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'}
crt = 'kickboxing.crt'

def get_code(link):
    return requests.get(url=link, headers=headers).status_code


def main():
    get_response = requests.get(url='http://kickboxing.md', headers=headers, verify=crt)
    soup = BS(get_response.text, "html.parser")
    images = soup.find_all('img')

    links = []
    for imgtag in images:
        if imgtag['src'].startswith('/'):
            links.append(''.join(('https://kickboxing.md', imgtag['src'])))
        else:
            links.append(imgtag['src'])

    # checking response using multithreading
    # how_many = 10
    # start = time.time()
    # pool = ThreadPool(processes=how_many)
    # codes = pool.map(func=get_code, iterable=links)
    # pool.close()
    # pool.join()
    # end = time.time()
    # print(end - start)
    #
    # for code in codes:
    #     if code != 200:
    #         print("Some links may not work!")

    req = requests.get(url=links[4], headers=headers, stream=True, verify=crt)
    with open('00000002.jpg', 'wb') as f:
        shutil.copyfileobj(req.raw, f)

    url = 'https://www.kickboxing.md/index.php'
    post_fields = {'searchword': 'cusnir', 'task': 'search',
                   'option': 'com_search', 'Itemid': '73'}
    html = requests.post(url, data=post_fields, headers=headers, verify=crt).text
    print(html)

if __name__ == "__main__":
    main()
