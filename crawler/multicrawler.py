import requests
from bs4 import BeautifulSoup as BS
from threading import Thread, Lock
from multiprocessing.pool import ThreadPool
import queue
import urllib.request

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'}
q = queue.Queue()
qf = queue.Queue()
tq = queue.Queue()
visited = []
found_urls = set()
count = 0
lock = Lock()


def put_urls(url):
    raw_links = set()
    parts = url.split('//', 1)
    url_domain = ''.join((parts[0], '//', parts[1].split('/', 1)[0]))
    try:
        get_response = requests.get(url, headers)
    except ConnectionError:
        pass
    soup = BS(get_response.text, "html.parser")
    found = soup.find_all('a')
    for a in found:
        try:
            if a['href'].startswith('//'):
                raw_links.add(a['href'])
            elif a['href'].startswith('/'):
                raw_links.add(''.join((url_domain, a['href'])))
            elif a['href'].endswith('.pdf') or a['href'].endswith('.docx'):
                pass
            else:
                raw_links.add(a['href'])
        except KeyError:
            pass
    for link in raw_links:
        if link.startswith('http'):
            q.put(link)


def th_get(word, url):
    try:
        # request = urllib.request.Request(url, None, headers)
        # text = str(urllib.request.urlopen(request).read())
        text = requests.get(url, headers).text
        return text.find(word)
    except urllib.error.HTTPError:
        print(url, "Connection error!")
        return -1


def crawl(word):
    global count
    while count < 100:  # check that the queue isn't empty
        with lock:
            count += 1
            url = q.get()  # get the item from the queue
        if url not in visited:
            put_urls(url)
            visited.append(url)
        pool = ThreadPool(processes=1)
        async_result = pool.apply_async(th_get, (word, url))
        return_val = async_result.get()
        if return_val > -1:
            qf.put(url)
        print(count, url, return_val)
        q.task_done()  # specify that you are done with the item


def main():
    url = 'https://google.md'
    word = "go"
    visited.append(url)
    put_urls(url)
    for i in range(10):  # aka number of threads
        t = Thread(target=crawl, args=(word,))  # target is the above function
        t.start()  # start the thread
        tq.put(t)
    while not tq.empty():
        tq.get().join()
    while not qf.empty():
        print(qf.get())

if __name__ == "__main__":
    main()
