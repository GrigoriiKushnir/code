﻿import os
directory = os.getcwdu()
path_f = []
for d, dirs, files in os.walk(directory):
    for f in files:
        path = os.path.join(d,f) # формирование адреса
        path_f.append(path) # добавление адреса в список

for f in path_f:
    base = os.path.splitext(f)[0]
    base = base.split('.')[0]
    extension = os.path.splitext(f)[1]
    f_d = open(f)
    b = f_d.read(2)
    f_d.close()
    if b.encode("hex") == '504b' and extension.lower() != '.docx':
        os.rename(f,base+".docx")
    if b.encode("hex") == '2550' and extension.lower() != '.pdf':
        os.rename(f,base+".pdf")
    if b.encode("hex") == 'd0cf' and extension.lower() != '.doc':
        os.rename(f,base+".doc")
    if b.encode("hex") == '7b5c' and extension.lower() != '.rtf':
        os.rename(f,base+".rtf")







